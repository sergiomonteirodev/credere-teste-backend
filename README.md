# Desafio Credere Backend

Projeto desenvolvido durante processo de seleção da Credere, onde consiste no seguinte desafio:

Uma sonda exploradora da NASA pousou em marte. O pouso se deu em uma área retangular, na qual a sonda pode navegar usando uma interface web. A posição da sonda é representada pelo seu eixo x e y, e a direção que ele está apontado pela letra inicial, sendo as direções válidas

## 🚀 Começando

Para o desafio utilizei a linguagem python==3.8.5, bem como seu microframework Flask==1.1.2 para mais informações sobre o flask acesse: https://flask.palletsprojects.com/.

O projeto esta estruturado com os seguintes diretórios:

- /core/sonda.py:

Contém toda a lógica necessária para o controle da sonda.
Utilizando o paradigma de Orientação a objeto.

- /static/swagger.json:

Contém as configurações da documentação swagger da API.

- /app.py:

Contém toda a lógica de interface para comunicação entre as requisições e o core.py
Nele também existe a configuração dos endpoints, recurso fornecido pelo microframework flask.

- /tests/*

Contém os testes unitários das apis.

### 📋 Pré-requisitos

Para rodar o projeto localmente é necessário ter instalado no S.O(Sistema operacional) o interpretador do python na versão 3.8.5, bem como, realizar a configuração de um ambiente virtual ou como chamamos popularmente de env.

*Aconselho criar um diretório/pasta para instalar e rodar o projeto.

### 🔧 Instalação

Caso não tenha o interpretador do python instalado no seu S.O(Sistema operacional).

1. Acessar o link abaixo na seção downloads:

```
https://www.python.org/downloads/
```
*OBS: Caso já tenha o python3.8.5, desconsiderar essa etapa.

2. Incialize o git:

```
git init
```

3. Clone o projeto executando o comando:

```
git clone https://gitlab.com/sergiomonteirodev/credere-teste-backend.git
```

3. Para criar um ambiente virtual:

- Rode o seguinte comando no terminal de sua preferência, dentro do diretório/pasta criado para o projeto:

```
python3 -m venv ./env

ou

python3 -m venv ./{NOME_DO_SEU_ENV}
```
*OBS: sugeri usar o nome env, pois é usado por convenção, mas fique a vontade para usar o nome que você quiser no seu ambiente virtual.

4. Ativar seu ambiente virtual:

```
source {NOME_DO_SEU_ENV}/bin/activate
```

5. Instalar as dependências do projeto:

```
pip install requirements.txt
```
*OBS: Digite o comando dentro da pasta do projeto, com o ambiente virtual ativado.

6. Executar o projeto :

```
flask run
```

*OBS: Digite o comando dentro da pasta do projeto, com o ambiente virtual ativado.

7. Realizar requisições no ambiente dev, localmente:

- Acessar a rota principal(http://127.0.0.1:5000) lá contém uma documentação swagger dos endpoints

- O projeto esta separado em 3 endpoints como solicita o desafio:

- O endpoint http://127.0.0.1:5000/posicao_inicial utiliza o metodo GET e retorna um json como reposta:

Ex:

```
{
  "x": 0,
  "y": 0
}
```

- O endpoint http://127.0.0.1:5000/coordenada_atual utiliza o metodo GET e retorna um json como resposta com a posição atual da sonda:

Ex:

```
{
  "x": 0,
  "y": 1
}
```

O endpoint http://127.0.0.1:5000/movimenta_sonda utiliza o metodo POST, recebe como requisição:

Ex:

```
{
  "movimentos": "['GE', 'M', 'M', 'GD', 'M', 'M', 'GD', 'M', 'M']"
}
```

E retorna um json:

Ex:

```
{
  "descrição": "A sonda girou para esquerda. andou 2 casas. A sonda girou para direita. andou 2 casas. A sonda girou para direita. andou 2 casas. ",
  "x": 2,
  "y": 0
}
```

## ⚙️ Executando os testes

Para executar os testes unitário das apis, rodar o seguinte comando no terminal:

```
pytest tests/tests_coordenada_atual_get.py

pytest tests/tests_movimenta_sonda_post.py

pytest tests/tests_reseta_sonda_get.py
```

## 📦 Desenvolvimento

O projeto em produção pode ser acessado via heroku: 

## 🛠️ Construído com

Mencione as ferramentas que você usou para criar seu projeto

* [python](https://www.python.org/downloads/) - linguagem
* [flask](https://flask.palletsprojects.com/) - O microframework web usado

## 📌 Versão

Utilizei [gitlab](https://gitlab.com/) para controle de versão.

## ✒️ Autores

Mencione todos aqueles que ajudaram a levantar o projeto desde o seu início

* **Sérgio MOnterio** - *Trabalho Inicial* - (sergiomonteirodev@gmail.com)


## 📄 Licença

Este projeto está sob a licença (MIT)

## 🎁 Expressões de gratidão

* Muito obrigado pela oportunidade 🤓.

---
