class Sonda:

    def __init__(self, x, y, direcao):
        self.x = x
        self.y = y
        self.direcao = direcao

    def reseta_posicao(self):
        self.x = 0
        self.y = 0
        self.direcao = 'D'

    # def movimenta_cima(self):
    #     if self.y == 4:
    #         return 'Movimento invalido'
    #     return self.y += 1

    def mover(self, movimentos):
        m = 0
        msg_erro = 'Movimento inválido. Sonda saiu do quadrante'
        texto = ''
        movimento_gd = {
            'D': 'B',
            'C': 'D',
            'E': 'C',
            'B': 'D'
        }

        movimento_ge = {
            'D': 'C',
            'C': 'E',
            'E': 'B',
            'B': 'E'
        }

        for pos in range(0, len(movimentos)):

            if movimentos[pos] == 'GD':
                self.direcao = movimento_gd[self.direcao]
                if m > 0:
                    texto += f'andou {m} casas. '
                    m = 0
                texto += 'A sonda girou para direita. '

            elif movimentos[pos] == 'GE':
                self.direcao = movimento_ge[self.direcao]
                if m > 0:
                    texto += f'andou {m} casas. '
                    m = 0
                texto += 'A sonda girou para esquerda. '

            elif movimentos[pos] == 'M':
                m += 1
                if self.direcao == 'D':
                    if self.x == 4: return msg_erro
                    self.x += 1

                elif self.direcao == 'E':
                    if self.x == 0: return msg_erro
                    self.x -= 1

                elif self.direcao == 'C':
                    if self.y == 4: return msg_erro
                    self.y += 1

                elif self.direcao == 'B':
                    if self.y == 0: return msg_erro
                    self.y -= 1
            else:
                return('Um movimento inválido foi detectado')

        if m > 0:
            texto += f'andou {m} casas. '

        return {'x': self.x, 'y': self.y, 'descrição': texto}
