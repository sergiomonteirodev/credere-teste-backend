import requests, json

def test_status_movimenta_sonda():
    headers = {
    'Accept': '*/*',
    'User-Agent': 'request',
    }

    url = "http://127.0.0.1:5000/movimenta_sonda"

    movimentos = {"movimentos" : "['GE', 'M', 'M', 'GD', 'M', 'M', 'GD', 'M', 'M']"}
    response = requests.post(url, headers=headers, json=movimentos)
    dados_resposta = response.json()

    assert response.status_code == 200
    assert dados_resposta['x'] == dados_resposta['x']
    assert dados_resposta['y'] == dados_resposta['y']
    assert dados_resposta['descrição'] == dados_resposta['descrição']
