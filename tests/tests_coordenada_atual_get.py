import requests

def test_coordenada_atual():
    headers = {
    'Accept': '*/*',
    'User-Agent': 'request',
    }

    url = "http://127.0.0.1:5000/coordenada_atual"

    response = requests.get(url, headers=headers)
    dados_resposta = response.json()

    assert response.status_code == 200
    assert dados_resposta['x'] == 0
    assert dados_resposta['y'] == 0
