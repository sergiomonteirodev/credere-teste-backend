from flask import Flask, render_template, url_for, jsonify, request, redirect
import json
from flask_swagger_ui import get_swaggerui_blueprint
from core.sonda import Sonda



app = Flask(__name__)

#inicializa a classe Sonda
sonda = Sonda(0, 0, 'D')

@app.route('/static/<path:path>')
def send_static():
    return send_from_directory('static', path)

SWAGGER_URL = '/doc'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Seans-Python-Flask-REST-Boilerplate"
    }
)

app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

@app.route('/')
def index():
    return redirect('/doc')

@app.route('/posicao_inicial', methods=['GET'])
def posicao_inicial():
    sonda.reseta_posicao()
    return jsonify({'x': sonda.x, 'y': sonda.y})

@app.route('/movimenta_sonda', methods=['POST'])
def movimenta_sonda():
    array = request.json['movimentos']
    data = eval(array)
    response = sonda.mover(data)
    return jsonify(response)

@app.route('/coordenada_atual', methods=['GET'])
def coordenada_atual():
    return jsonify({'x': sonda.x, 'y': sonda.y})
